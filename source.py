class Factory:
	def create_complex(self, a, b):
		pass

	def create_number(self, number):
		pass


class ComplexInt:
	def __init__(self, a, b):
		self.number = complex(a, b)


class ComplexN:
	def __init__(self, a, b, n):
		self.a = a%n
		self.b = b%n
		self.n = n
		self.number = complex(a%n, b%n)

	def __add__(self, other):
		return complex((self.a+other.a)%self.n, (self.b+other.b)%self.n)

	# TODO: вычитание, умножение и деление


class ComplexFive(ComplexN):
	def __init__(self, a, b):
		super().__init__(a, b, 5)


class ComplexSeven(ComplexN):
	def __init__(self, a, b):
		super().__init__(a, b, 7)


class OrdInt:
	def __init__(self, a):
		self.number = a


class OrdN:
	def __init__(self, a, n):
		self.n = n
		self.number = a%n

	def __add__(self, other):
		return (self.number+other.number)%self.n

	# TODO: вычитание, умножение и деление


class OrdFive(OrdN):
	def __init__(self, a):
		super().__init__(a, 5)


class OrdSeven(OrdN):
	def __init__(self, a):
		super().__init__(a, 7)



class FactoryInt(Factory):
	def create_complex(self, a, b):
		return ComplexInt(a, b)

	def create_number(self, number):
		return OrdInt(number)


class FactoryFive(Factory):
	def create_complex(self, a, b):
		return ComplexFive(a, b)

	def create_number(self, number):
		return OrdFive(number)

class FactorySeven(Factory):
	def create_complex(self, a, b):
		return ComplexSeven(a, b)

	def create_number(self, number):
		return OrdSeven(number)


if __name__ == '__main__':



	factorySeven = FactorySeven()
	print(factorySeven.create_complex(5, 8).number)
	print(factorySeven.create_number(5).number)

	print(factorySeven.create_complex(5, 8) + factorySeven.create_complex(6, 8))
	print(factorySeven.create_number(5) + factorySeven.create_number(6))